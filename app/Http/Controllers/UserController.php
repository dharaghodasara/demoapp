<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function viewapproval()
    {
        return view('admin.approval');
    }
    public function index()
    {
        $users = User::where('admin',0)->get();

        return view('admin.users', compact('users'));
    }

    public function approve($user_id)
    {
        $user = User::findOrFail($user_id);
        $user->update([
            'status'=> '1',
            'approved_at' => date('Y-m-d H:i:s'),
            ]);

        return redirect()->route('admin.users.index')->withMessage('User approved successfully');
    }
    public function reject($user_id)
    {
        $user = User::findOrFail($user_id);
        $user->update(['status' => 0]);

        return redirect()->route('admin.users.index')->withMessage('User Rejected successfully');
    }
}
