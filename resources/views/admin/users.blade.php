@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-4">
                <div class="card border-success">
                    <div class="card-header">Users </div>

                    <div class="card-body justify-center">

                        @if (session('message'))
                            <div class="alert alert-success" role="alert">
                                {{ session('message') }}
                            </div>
                        @endif

                        <table class="table table-bordered">
                            <tr >
                                <th>User name</th>
                                <th>Email</th>
                                <th>Registered at</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            @forelse ($users as $user)
                                <tr  {{$user->status == 1 ? 'style="color:green"' : 'style="color:red"' }}>
                                    <td>{{ $user->name }} </td>
                                    <td>{{ $user->email }}</td>
                                   
                                    <td>{{date('d-m-Y H:i:s',strtotime($user->created_at)) }}</td>
                                    <td>{{($user->status == 0) ? 'Rejected' : (($user->status == 1) ? 'Approved' : 'Pending') }}</td>
                                    <td>@if($user->status != 1)
                                        <a href="{{ route('admin.users.approve', $user->id) }}"
                                           class="btn btn-success btn-sm">Approve</a>
                                        @else
                                           <a href="{{ route('admin.users.reject', $user->id) }}"
                                           class="btn btn-danger btn-sm">Reject</a>
                                        @endif</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">No users found.</td>
                                </tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection