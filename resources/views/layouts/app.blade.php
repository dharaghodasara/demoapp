<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="900;url=logout" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Demo App</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('app.css')}}" rel="stylesheet">

    <link href="{{asset('css/app.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
    <div id="app">
    <body >
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <!-- <a class="navbar-brand" href="#">Container</a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        @if (Auth::guest())
        <div class="collapse navbar-collapse" id="navbarsExample07">
            <ul class="navbar-nav mr-auto">
               
            
                <li class="nav-item active">
                <a class="nav-link" href="{{ route('login') }}">Login <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                <a class="nav-link" href="{{ route('register') }}">Register</a>
                </li>
            </ul>
        </div>
            @else
            <div class="form-inline my-2 my-md-0" style="float:right;">
                <!-- <input class="form-control" type="text" placeholder="Search"> -->
                </div>
                <a class="nav-link" href="{{ route('logout') }}"style="color:white;" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                </form>
            @endif
                </div>
            </div>
        </nav>
   
        <div class="container-fluid">
        <div class="row">
        @if(!Auth::guest())

            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link " href="{{ url('/home')}}">
                        <span data-feather="home"></span>
                        Dashboard <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    @if(isset(Auth::user()->admin))
                            @if (Auth::user()->admin == 1)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/users') }}">
                                <span data-feather="file"></span>
                                Users
                                </a>
                            </li>
                            @endif
                        @endif
                    
                    </ul>

                </div>
            </nav>
        @endif
            @yield('content')
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
